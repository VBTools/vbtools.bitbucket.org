$(document).ready(function() {
	var headerheight = jQuery('.header').height();
	if (headerheight < 153) {
		jQuery('.wrap').css('margin','12.5px !important');
		jQuery('.scroll .wrap').css('margin','6.25px !important');
		jQuery('.content').css('margin','153px auto 0');
	} else {
		jQuery('.content').css('margin',headerheight+'px auto 0');
	}
	$('img').on('dragstart', function(event) { event.preventDefault(); });
	$(window).scroll(function() {
		console.log($(window).scrollTop())
		if ($(window).scrollTop() > headerheight) {
			$('.logo').addClass('scroll');
		}
		if ($(window).scrollTop() < headerheight) {
			$('.logo').removeClass('scroll');
		}
	});
	
	if(localStorage.getItem("firstTime")==null) {
		$('.welcome').css('display','block');
		$('.nav-ul').css('border-top','1px solid #ddd');
	}
});

function goTop() {
	$('html, body').animate({ scrollTop: 0 }, 'fast');
}

function closeBox() {
	localStorage.setItem("firstTime","done");
	$('.welcome').css('opacity','0');
	$('.nav-ul').css('border-top','1px solid rgba(221,221,221,0)');
	setTimeout("$('.welcome').css('display','none');", 250);
	$('.nav-ul').css('border-top','0px solid rgba(221,221,221,0)');
}

$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-63781692-1', 'auto');
ga('send', 'pageview');